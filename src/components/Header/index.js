import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Link } from 'react-router';

// constants

// sub-components
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import ActionHome from 'material-ui/svg-icons/action/home';
import AppBar from 'material-ui/AppBar';

// action

// redux
function mapStateToProps(state) {
    return {
    };
}

function mapDispatchToProps(dispatch) {
    return ({
    });
}

// style
var style= {
}

// component
class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      logged: false,
    }
  }

  render(){
    return(
      <div className="header"
        style={{
          flex: '0 0 64px',
        }}
      >
        <AppBar
          title="Title"
          iconElementLeft={<IconButton><ActionHome /></IconButton>}
          iconElementRight={this.state.logged ?
              <FlatButton style={this.props.style}
                onClick={(e)=>{
                  this.setState({logged: !this.state.logged})
                }}
                label="LOG OUT"
              />
              : 
              <FlatButton label="LOG IN"
                onClick={(e)=>{
                  this.setState({logged: !this.state.logged})
                }}
              />}
        />
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
