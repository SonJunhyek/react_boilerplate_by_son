import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import crawl from '../utils';

// constants

// sub-components

// action

// redux
function mapStateToProps(state) {
    return {
    };
}

function mapDispatchToProps(dispatch) {
    return ({
    });
}

var varName ="this";
// style
var style= {
}

// component
class Home extends React.Component {
  constructor(props) { super(props); }

  render(){
    return(
      <div className="app-body"
        style={{
          width: '100%',
          height: 'inherit',
          display: 'flex',
          flexDirection: 'column',
        }}
        className={varName}
      >
        <button onClick={() => {crawl()}} > request </button>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
