import axios from 'axios';

let MAIN_API = "http://api.welcome.kakao.com"; // + {category}+ "/" + {docid}
let TOKEN_API = "http://api.welcome.kakao.com/token/"; // + TOKEN
let SEED_API = "http://api.welcome.kakao.com/seed";
let FEATURE_API = "http://api.welcome.kakao.com/image/feature";
let SON_LOGIN_TOKEN = "UrKDkOm25R5b2FBnGxO5BbO-qHVdNV8DX10yJfQN";

let crawl = async () => {
  let loginToken = SON_LOGIN_TOKEN;
  let categoryNames = [];

  // GET TOKEN
  let token = await axios.get(TOKEN_API + loginToken)
    .then((res) => {
      let token = ""
      console.log("success to get a token :" + res.data);
      token = res.data;
      return token
    })
    .catch((error) => {
      let token = ""
      if (error.response) {
        if (error.response.status == 403) { // in case, token have been already issued
          token = error.response.data
          console.log("token have been already issued : " + token );
          return token
        }
        console.log('data : ' + error.response.data);
        console.log('status : ' +error.response.status);
        console.log('header : ' + JSON.stringify(error.response.headers));
      } else if (error.request) {
        console.log(error.request);
      } else {
        console.log('Error', error.message);
      }
    })

  // MAKE CONFIG WITH TOKEN
  let config = {
    headers: {
      "X-Auth-Token": token
    }
  }

  // GET CATEGORIES
  let categories = await axios.get(SEED_API, config)
    .then((res) => {
      let categories = res.data.split("\n");
      let categoryDic = {}
      for (let category of categories) {
        let name = category.split('/')[2];
        if (name) {
          categoryNames.push(name);
          categoryDic[name] = category;
        }
      }
      console.log("success to get categories :" + JSON.stringify(categoryDic));
      return categoryDic
    })
    .catch((error) => {
      console.log("fail to get categories :" + error);
    });

  // GET DOCUEMNTS
  let imageDictionary = {
    processed: {}, // 처리된 이미지
    ready: {} // 처리 안된 이미지
  };

  for (let key in categories) {
    let additinalUrl = categories[key];
    let SEED = await axios.get(MAIN_API + additinalUrl, config)
      .then((res) => {
        let data = res.data;
        let nextUrl = data.next_url;
        let images = data.images;
        if (!imageDictionary[key]) {imageDictionary[key] = [nextUrl]}
        for (let image of images) {
          imageDictionary.ready[image.id] = {type: image.type, category: key, feature: null, requested: false};
        }

        console.log("success to get images info :" + JSON.stringify(data));
      })
      .catch((error) => {
        console.log("fail to get images info :" + error);
      });
  }
  let repeat = () => {

  }



  /*
   * 최초에 얻은 카테고리 리스트를 바탕으로 아래에서 반복적으로 사이트를 찾으며 이미지를 처리합니다.
   */
  let index = 0
  while (index < 20) {
    // 블로그 이동하며 이미지 정보 불러오기
    let targetCategory = categoryNames[index % categoryNames.length];
    let targetUrls = imageDictionary[targetCategory];
    let urlNum = targetUrls.length;
    let targetUrl = targetUrls[urlNum - 1];

    let isRepeat = false;
    while(!isRepeat) {
      let wait = await axios.get(MAIN_API+targetUrl, config)
        .then((res) => {
          let data = res.data;
          let nextUrl = data.next_url;
          let images = data.images;
          if ( nextUrl == targetUrl && images.length == 0) { // 아직 크로울링 되기 전일 때
            isRepeat = true
            index += 1; // 다음 카테고리에 대해 요청
          }
          else {
            imageDictionary[targetCategory].push(nextUrl)
            for (let image of images) {
              imageDictionary.ready[image.id] = {
                type: image.type, category: targetCategory, feature: null, requested: false
              };
            }
          }

          console.log("success to get images info :" + JSON.stringify(data));
        })
        .catch((error) => {
          console.log("fail to get images info :" + error);
        });
    }


    // 가져온 이미지 중 처리 안된 이미지 처리하기
    // 처리 안된 이미지(ready) 중 최고 50개까지 각 버퍼에 삽입하고 이를 processed로 넘김

    let imageToAdd = [];
    let imageToDelete = [];
    let imageIdsOnReady = imageDictionary.ready
    let readyImageNum = Object.keys(imageIdsOnReady).length

    while( readyImageNum > 0 ) {
      for (let id in imageIdsOnReady) {
        if ( imageIdsOnReady[id].type == 'add' ) {
          if(imageToAdd.length < 50 && readyImageNum !== 0 ) {
            imageToAdd.push(id);
            imageDictionary.processed[id] = imageIdsOnReady[id];
            delete imageIdsOnReady[id];
          }

        } else if ( imageIdsOnReady[id].type == 'del' ) {
          if(imageToDelete.length < 50 && readyImageNum !== 0 ) {
            imageToDelete.push(id);
            imageDictionary.processed[id] = imageIdsOnReady[id];
            delete imageIdsOnReady[id];
          }
        }

      }

      imageIdsOnReady = imageDictionary.ready
      readyImageNum = Object.keys(imageIdsOnReady).length

      if( imageToAdd.length == 50 || (readyImageNum == 0 && imageToAdd.length !== 0) ) {
        // feature request
        let featureApi = FEATURE_API + '?id=' + imageToAdd.join(',');
        // 초기화
        imageToAdd = [];
        let features = await axios.get( featureApi , config)
          .then((res) => {
            let features = res.data.features;
            for (let afeature of features) {
              let {id, feature} = afeature;
              imageDictionary.processed[id]['feature'] = feature;
            }
            return features
            console.log("success to get features info :" + JSON.stringify(features));
          })
          .catch((error) => {
            console.log("fail to get features info :" + error);
          });
      }

      if( imageToDelete.length == 50 || (readyImageNum == 0 && imageToDelete.length !== 0) ) {
        let deletedFeatures = [];
        for( let id of imageToDelete ) {
          imageDictionary.processed[id].requested = true;
          deletedFeatures.push({id: id});
        }
        // delete request TODO await 필요 없음 (테스트용임)
        let deleteConfig = {
          data: {
            data: deletedFeatures
          },
          headers: {
            "X-Auth-Token": token
          }
        }

        let deleteFeature = await axios.delete(FEATURE_API, deleteConfig )
          .then((res) => {
            let status = res.status;
            console.log("success to delete :" + status);
          })
          .catch((error) => {
            console.log("fail to delete :" + error);
          });
        imageToDelete = [];
      }
    }

    // save request TODO await 필요 없음 (테스트용임)
    let savedFeatures = [];
    let processedIds = imageDictionary.processed;
    for ( let imageId in processedIds ) {
      let imageInfo = processedIds[imageId];
      if ( imageInfo.type == 'add' && !imageInfo.requested && imageInfo.feature) {
        imageInfo.requested = true;
        savedFeatures.push({id: imageId, feature: imageInfo.feature})
      }
    }

    while ( savedFeatures.length > 0 ) {
      let min = Math.min(50,savedFeatures.length);
      let savedFeaturesData = savedFeatures.splice(0, min);
      savedFeatures = savedFeatures.splice(min);
      let saveFeature = await axios.post(FEATURE_API, {data: savedFeaturesData}, config )
        .then((res) => {
          let features = res.data.features;
          return features
          console.log("success to save :" + res);
        })
        .catch((error) => {
          console.log("fail to save :" + error);
        });
    }
    //let div = document.createElement('div')
    //div.innerText = JSON.stringify(imageDictionary);
    //document.getElementById('result').appendChild(div);
    // delete 요청
  }
}

export default crawl

/*
axios.get('/user/12345')
  .then(function(response) {
    console.log(response.data);
    console.log(response.status);
    console.log(response.statusText);
    console.log(response.headers);
    console.log(response.config);
  })
  .catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
    }
    console.log(error.config);
  });
*/
