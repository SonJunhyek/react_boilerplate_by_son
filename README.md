
* 개발 환경
개발환경은 http 요청에 대하여 브라우저에서 데이터 요청에 대한 처리를 바로바로 확인하고 디버깅하는 것이 쉬울 것이라 판단하여 리액트를 활용하였습니다.

* 실행 방법
node(6.11.4) download 후, root directory에서 다음을 입력합니다.
$ npm install // 모듈 다운로드
$ npm start // webpack dev 모드 실행
서버가 켜지면 localhost:7777/home 으로 메인 페이지 접근이 가능하며, 가운데 request 버튼을 누르면 실행됩니다.
구현은 ~/src/utils/index.js 에 되어 있으며, ~/src/componets/Home.js 컴포넌트에서 해당 함수를 import 하여 실행하는 방식입니다

* 구현
- axios라는 ajax library를 활용하였으며, promise 를 통해 async 요청을 순차적으로 처리하였습니다.
- 주어진 api들에 대한 요청들을 처리할 수 있습니다.
- data structure에 dictionary를 사용하여, 중복된 image와 id key를 통한 내부에 저장한 데이터의 조회하기 쉽도록 하였습니다.

* 구현 한계
요청 횟수의 제약 (초당 50회)에 대한 처리를 구현하지 못하였습니다.
